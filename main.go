package main

import (
	"bufio"
	"bytes"

	//"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/controllercubiomes/util"
)

//const url = "http://controller.sevisapickyasshole.net"
const domain = "seedhunt.net"
const url = "https://" + domain + "/controller"
const api = "https://" + domain + "/api"
const seedUri = "https://" + domain + "/seed/"

var tag = "v1.0.0"

type Key struct {
	Key string `json:"key"`
}

func toInt64(s string) (i int64) {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.Println(err)
	}
	return
}

func getChunk(client *http.Client, criteria, key string) (chunk util.Chunk, err error) {
	req, err := http.NewRequest("GET", url+"/chunk/"+criteria, nil)
	if err != nil {
		return
	}
	req.Header.Set("authorization", key)
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		log.Fatalf("Error from server, is your criteria valid? errorcode: %d", resp.StatusCode) //TODO: actually handle
	}

	err = json.NewDecoder(resp.Body).Decode(&chunk)
	return
}

func getKey(client *http.Client) (key string, err error) {
	req, err := http.NewRequest("GET", api+"/registersystem", nil)
	if err != nil {
		return
	}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		log.Fatalf("Error from server, is your criteria valid? errorcode: %d", resp.StatusCode) //TODO: actually handle
	}
	var tmpKey Key
	err = json.NewDecoder(resp.Body).Decode(&tmpKey)
	key = tmpKey.Key
	return
}

func main() {
	var criteria string
	var threads int
	var isUseLocalSearcher bool
	flag.StringVar(&criteria, "c", "test", "Criteria to search for") //TODO: remove thest and add back user interface for choosing criteria
	flag.IntVar(&threads, "t", runtime.NumCPU(), "Number of threads you want to use. You have "+string(runtime.NumCPU())+" threads available")
	flag.BoolVar(&isUseLocalSearcher, "d", false, "[Advanced Developer Option] Use local searcher instead of remote searcher. Do NOT use this option if you don't know what you're doing!")
	flag.StringVar(&tag, "v", tag, "[Advanced Developer Option] Use special tag for testing. Do NOT use this option if you don't know what you're doing!")
	flag.Parse()
	if len(os.Args) > 1 && criteria == "" {
		fmt.Println("When using args you need to also give a criteria with the arg -c")
		fmt.Println("For a full list of args run the program with arg --help")
		fmt.Println("You can also run the program without any arguments")
		return
	} else if len(os.Args) == 1 {
		reader := bufio.NewReader(os.Stdin)
		for criteria == "" {
			fmt.Print("Enter criteria you want to help search for: ")
			criteria, _ = reader.ReadString('\n')
			criteria = strings.TrimSpace(criteria)
		}
		fmt.Printf("Enter the amount of threads you want to use. You have (%d) available: ", runtime.NumCPU())
		threadsString, _ := reader.ReadString('\n')
		var err error
		threads, err = strconv.Atoi(strings.TrimSpace(threadsString))
		if err != nil {
			log.Fatal(err)
		}
	}
	if threads < 1 {
		log.Println("Number of threads selected were less than 1, defaulting to 1 thread for this execution...")
		threads = 1
	} else if threads > runtime.NumCPU() {
		log.Printf("Number of threads selected exceeds your maximum threads! Defaulting to the maximum threads you have available which is %d", runtime.NumCPU())
		threads = runtime.NumCPU()
	}
	client := &http.Client{}
	key, err := getKey(client)
	if err != nil || key == "" {
		log.Fatal("Could not register system with err: \n", err)
	}
	newChunks := make(chan util.Chunk)
	go func() {
		chunk, err := getChunk(client, criteria, key)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(chunk)
		// If the user does not want to use their own searcher, download the remote one
		if !isUseLocalSearcher {
			if err := DownloadFile("./searcher", chunk.ProgramURL); err != nil {
				log.Fatal(err)
			}
		}
		newChunks <- chunk
		for {
			chunk, err = getChunk(client, criteria, key)
			if err != nil {
				log.Println(err)
				return
			}
			newChunks <- chunk
		}
	}()
	for {
		chunk := <-newChunks
		chunk.SeedsFound = 0
		log.Printf("Starting chunk: %d -> %d!", chunk.Start, chunk.End)
		newSeed := make(chan util.Initial, 1)
		var cmd *exec.Cmd
		if runtime.GOOS == "windows" {
			cmd = exec.Command("searcher.exe", strconv.FormatInt(chunk.Start, 10), strconv.FormatInt(chunk.End, 10), strconv.Itoa(threads), strconv.Itoa(chunk.RadiusInner), strconv.Itoa(chunk.RadiusOuter), "n", "1")
		} else {
			cmd = exec.Command("./searcher", strconv.FormatInt(chunk.Start, 10), strconv.FormatInt(chunk.End, 10), strconv.Itoa(threads), strconv.Itoa(chunk.RadiusInner), strconv.Itoa(chunk.RadiusOuter), "n", "1")
		}
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatal(err)
		}
		stderr, err := cmd.StderrPipe()
		if err != nil {
			log.Fatal(err)
		}

		err = cmd.Start()
		if err != nil {
			log.Fatal(err)
		}

		go io.Copy(os.Stdout, stderr)
		go readStdout(stdout, newSeed)

		go func() {
			err = cmd.Wait()
			if err != nil {
				log.Println(err)
			}
			close(newSeed)
		}()

		chunk.SeedsFound = 0
		for seed := range newSeed {
			seed.Criteria = criteria
			seed.Finder = key
			data, err := json.Marshal(seed)
			if err != nil {
				log.Println(err)
				continue
			}
			sent := false
			for !sent {
				req, err := http.NewRequest("POST", url+"/seed", bytes.NewBuffer(data))
				if err != nil {
					log.Println(err)
					continue
				}
				req.Header.Set("authorization", key)
				client := &http.Client{}
				_, err = client.Do(req)
				if err != nil {
					log.Println(err)
					continue
				}
				sent = true
			}
			fmt.Printf("\nFind your seed at : %s%d\n", seedUri, seed.Seed)
			chunk.SeedsFound++
		}
		chunk.Key = key
		data, err := json.Marshal(chunk)
		if err != nil {
			log.Println(err)
		}
		sent := false
		for !sent {
			req, err := http.NewRequest("POST", url+"/chunkdone", bytes.NewBuffer(data))
			if err != nil {
				log.Println(err)
				continue
			}
			req.Header.Set("authorization", key)
			_, err = client.Do(req)
			if err != nil {
				log.Println(err)
				continue
			}
			sent = true
		}
		log.Printf("Finished chunk: %d -> %d with %d seeds found!", chunk.Start, chunk.End, chunk.SeedsFound)
	}
}

func readStdout(stdout io.ReadCloser, newSeed chan<- util.Initial) {
	defer stdout.Close()
	scanner := bufio.NewScanner(stdout)
	for scanner.Scan() {
		seed := util.Initial{
			Seed: toInt64(scanner.Text()[1:]),
		}
		newSeed <- seed
	}
	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
}

func DownloadFile(filepath string, url string) error {
	// Get the data
	url = strings.ReplaceAll(url, "<tag>", tag)
	url = strings.ReplaceAll(url, "<file>", filepath[2:])
	switch os := runtime.GOOS; os {
	case "windows":
		url = url + ":" + os
		filepath = filepath[2:] + ".exe"
	case "linux":
		switch arch := runtime.GOARCH; arch {
		case "amd64":
			url = url + ":" + os
		case "arm64":
			url = url + ":" + os + ":" + arch
		case "arm":
			url = url + ":" + os + ":" + arch
		default:
			log.Fatal("Your architecture is not supported")
		}
	case "darwin":
		url = url + ":" + os
	default:
		log.Fatal("Your os is not supported")
	}
	log.Println("Temp log for file download:\n", url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
